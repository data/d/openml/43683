# OpenML dataset: WebMD-Drug-Reviews-Dataset

https://www.openml.org/d/43683

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The dataset provides user reviews on specific drugs along with related conditions, side effects, age, sex, and ratings reflecting overall patient satisfaction.
Content
Data was acquired by scraping WebMD site. There are around 0.36 million rows of unique reviews and is updated till Mar 2020.
Inspiration
This dataset intended to answer following questions:
I. Identifying the condition of the patient based on drug reviews?
II. How to predict drug rating based on patients reviews?
III. How to visualize drug rating, kind of drugs, types of conditions a patient can have, sentiments based on reviews

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43683) of an [OpenML dataset](https://www.openml.org/d/43683). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43683/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43683/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43683/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

